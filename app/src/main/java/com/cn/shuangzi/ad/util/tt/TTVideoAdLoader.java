package com.cn.shuangzi.ad.util.tt;

import android.app.Activity;
import android.os.Bundle;

import com.bytedance.sdk.openadsdk.AdSlot;
import com.bytedance.sdk.openadsdk.TTAdConstant;
import com.bytedance.sdk.openadsdk.TTAdManager;
import com.bytedance.sdk.openadsdk.TTAdNative;
import com.bytedance.sdk.openadsdk.TTAppDownloadListener;
import com.bytedance.sdk.openadsdk.TTRewardVideoAd;
import com.cn.shuangzi.ad.util.RewardBundleModel;
import com.cn.shuangzi.ad.util.VideoAdLoader;
import com.cn.shuangzi.util.SZUtil;

/**
 * Created by CN.
 */

public class TTVideoAdLoader {
    private TTAdNative mTTAdNative;
    private TTRewardVideoAd mttRewardVideoAd;
    private VideoAdLoader.VideoPlayListener videoPlayListener;
    private String codId;
    private Activity activity;
    private boolean isRewardSuccess;
    private boolean isDealDoneAfterReward;
    public TTVideoAdLoader(Activity activity, String codId, VideoAdLoader.VideoPlayListener videoPlayListener) {
        this.videoPlayListener = videoPlayListener;
        this.activity = activity;
        this.codId = codId;
        TTAdManager ttAdManager = TTAdManagerHolder.getInstance();
        mTTAdNative = ttAdManager.createAdNative(activity);
        isRewardSuccess = false;
        isDealDoneAfterReward = false;
    }

    public void loadVideo() {
        loadVideo(null,null);
    }
    public void loadVideo(String userId,String extra) {
        isRewardSuccess = false;
        isDealDoneAfterReward = false;
        AdSlot adSlot = new AdSlot.Builder()
                .setCodeId(codId)
                .setSupportDeepLink(true)
//                .setDownloadType(TTAdConstant.DOWNLOAD_TYPE_POPUP)
                .setImageAcceptedSize(1080, 1920)
                .setUserID(userId)
                .setMediaExtra(extra)
                .setExtraParam(extra).setOrientation(TTAdConstant.VERTICAL)
                .build();
        mTTAdNative.loadRewardVideoAd(adSlot, new TTAdNative.RewardVideoAdListener() {
            @Override
            public void onError(int code, String message) {
                SZUtil.log("code:"+code+"||msg:"+message);
                if(videoPlayListener!=null){
                    videoPlayListener.onError();
                }
            }

            public void onRewardVideoCached() {
            }

            @Override
            public void onRewardVideoCached(TTRewardVideoAd ttRewardVideoAd) {

            }

            public void onRewardVideoAdLoad(TTRewardVideoAd ad) {
                mttRewardVideoAd = ad;
                mttRewardVideoAd.setRewardAdInteractionListener(new TTRewardVideoAd.RewardAdInteractionListener() {

                    @Override
                    public void onAdShow() {
                        SZUtil.log("展示广告了=====onAdShow======");
                        if(videoPlayListener!=null){
                            videoPlayListener.onShow();
                        }
                    }

                    @Override
                    public void onAdVideoBarClick() {
                    }

                    @Override
                    public void onAdClose() {
                        SZUtil.log("关闭广告了=====onAdClose======");
                        if(videoPlayListener!=null){
                            videoPlayListener.onClose();
                        }
                    }

                    @Override
                    public void onVideoComplete() {
                        SZUtil.log("====onVideoComplete====");
                    }

                    @Override
                    public void onVideoError() {
                        if(videoPlayListener!=null){
                            videoPlayListener.onError();
                        }
                    }

                    @Override
                    public void onRewardVerify(boolean rewardVerify, int rewardAmount, String rewardName, int i1, String s1) {
//                        isRewardSuccess = rewardVerify;
                        SZUtil.log("rewardVerify:"+rewardVerify+"|rewardAmount:"+rewardAmount+"|rewardName:"+rewardName
                                +"|errorCode:"+i1+"|errorMsg:"+s1);
//                        if(videoPlayListener!=null){
//                            if(rewardVerify) {
//                                videoPlayListener.onSuccess();
//                            }else{
//                                videoPlayListener.onError();
//                            }
//                        }
                    }

                    @Override
                    public void onRewardArrived(boolean isRewardValid, int rewardType, Bundle extraInfo) {

                        RewardBundleModel rewardBundleModel = new RewardBundleModel(extraInfo);
                        isRewardSuccess = isRewardValid;
                        if(videoPlayListener!=null){
                            if(isRewardValid) {
                                videoPlayListener.onSuccess();
                            }else{
                                videoPlayListener.onError();
                            }
                        }
                        SZUtil.logError("Callback --> rewardVideoAd has onRewardArrived " +
                                "\n奖励是否有效：" + isRewardValid +
                                "\n奖励类型：" + rewardType +
                                "\n奖励名称：" + rewardBundleModel.getRewardName() +
                                "\n奖励数量：" + rewardBundleModel.getRewardAmount() +
                                "\n建议奖励百分比：" + rewardBundleModel.getRewardPropose());
                        if (!isRewardValid) {
                            SZUtil.logError("发送奖励失败 code：" + rewardBundleModel.getServerErrorCode() +
                                    "\n msg：" + rewardBundleModel.getServerErrorMsg());
                            return;
                        }

//                        if (!isEnableAdvancedReward) {
                        // 未使用进阶奖励功能
                        if (rewardType == TTRewardVideoAd.REWARD_TYPE_DEFAULT) {
                            SZUtil.logError("普通奖励发放，name:" + rewardBundleModel.getRewardName() +
                                    "\namount:" + rewardBundleModel.getRewardAmount());
                        }
//                        } else {
//                            // 使用了进阶奖励功能
//                            if (mRewardAdvancedInfo != null) {
//                                mRewardAdvancedInfo.proxyRewardModel(rewardBundleModel, false);
//                            }
//                        }
                    }

                    @Override
                    public void onSkippedVideo() {
                        if(videoPlayListener!=null){
                            videoPlayListener.onSkip();
                        }
                    }
                });
                mttRewardVideoAd.setDownloadListener(new TTAppDownloadListener() {
                    @Override
                    public void onIdle() {
                    }

                    @Override
                    public void onDownloadActive(long totalBytes, long currBytes, String fileName, String appName) {
                    }

                    @Override
                    public void onDownloadPaused(long totalBytes, long currBytes, String fileName, String appName) {
                    }

                    @Override
                    public void onDownloadFailed(long totalBytes, long currBytes, String fileName, String appName) {
                    }

                    @Override
                    public void onDownloadFinished(long totalBytes, String fileName, String appName) {
                    }

                    @Override
                    public void onInstalled(String fileName, String appName) {
                    }
                });
                mttRewardVideoAd.showRewardVideoAd(activity);
            }
        });
    }

    public void setDealDoneAfterReward(boolean dealDoneAfterReward) {
        isDealDoneAfterReward = dealDoneAfterReward;
    }

    public boolean isDealDoneAfterReward() {
        return isDealDoneAfterReward;
    }

    public boolean isRewardSuccess() {
        return isRewardSuccess;
    }

    public void setRewardSuccess(boolean rewardSuccess) {
        isRewardSuccess = rewardSuccess;
    }
}
