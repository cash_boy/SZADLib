package com.cn.shuangzi.ad.bean;

import com.cn.shuangzi.bean.MaterialsInfo;
import com.google.gson.Gson;

import java.io.Serializable;

/**
 * Created by CN.
 */

public class ADPlatform implements Serializable{
    public static final String AD_TYPE_SPLASH = "LAUNCH_AD";
    public static final String AD_TYPE_NATIVE = "INFORMATION_FLOW";
    public static final String AD_TYPE_GIFT_BOX = "GIFT_BOX_AD";
    public static final String AD_TYPE_BANNER = "BANNER_AD";
    public static final String AD_TYPE_SELF = "SELF_SUPPORT";
    public static final String AD_TYPE_INTERACTION = "INTERACTION_AD";
    private boolean adIsOpen;
    private String adSupportType;
    private String adExtendData;
    private String adType;
    private String adVid;

    public boolean isSelfAd(){
        return AD_TYPE_SELF.equalsIgnoreCase(adSupportType);
    }
    public boolean isAdIsOpen() {
        return adIsOpen;
    }

    public String getAdSupportType() {
        return adSupportType;
    }

    public String getAdType() {
        return adType;
    }

    public String getAdExtendData() {
        return adExtendData;
    }

    public MaterialsInfo getMaterialsInfo(){
        try {
            return new Gson().fromJson(adExtendData,MaterialsInfo.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public AdShowTypeInfo getAdShowTypeInfo(){
        try {
            return new Gson().fromJson(adExtendData,AdShowTypeInfo.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String toString() {
        return "ADPlatform{" +
                "adIsOpen=" + adIsOpen +
                ", adSupportType='" + adSupportType + '\'' +
                ", adExtendData='" + adExtendData + '\'' +
                ", adType='" + adType + '\'' +
                ", adVid='" + adVid + '\'' +
                '}';
    }
}
