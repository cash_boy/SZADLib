package com.cn.shuangzi.ad.util.gdt;

import com.androidquery.callback.BitmapAjaxCallback;
import com.qq.e.comm.constants.LoadAdParams;
import com.qq.e.comm.managers.status.SDKStatus;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by CN.
 */
public class GDTUtil {
    private static boolean sNeedSetBidECPM = false;

    public static void setAQueryImageUserAgent(){
        BitmapAjaxCallback.setAgent("GDTMobSDK-AQuery-"+ SDKStatus.getIntegrationSDKVersion());
    }

    public static void setNeedSetBidECPM(boolean need) {
        sNeedSetBidECPM = need;
    }

    public static boolean isNeedSetBidECPM() {
        return sNeedSetBidECPM;
    }

    static LoadAdParams getLoadAdParams(String value) {
        Map<String, String> info = new HashMap<>();
        info.put("custom_key", value);
        LoadAdParams loadAdParams = new LoadAdParams();
        loadAdParams.setDevExtra(info);
        return loadAdParams;
    }

    public static boolean isAdValid(boolean loadSuccess, boolean isValid, boolean showAd) {
        if (!loadSuccess) {
            return false;
        } else {
            if (!showAd || !isValid) {
                return false;
            }
        }
        return isValid;
    }
}
