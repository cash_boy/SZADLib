package com.cn.shuangzi.ad;

import android.os.Bundle;

import com.cn.shuangzi.SZBaseActivity;
import com.cn.shuangzi.ad.ADManager;
import com.cn.shuangzi.ad.util.OnAdClosedListener;
import com.cn.shuangzi.ad.util.TTVideoInteractionAdLoader;
import com.cn.shuangzi.ad.util.tt.SplashClickEyeManager;
import com.cn.shuangzi.util.SZUtil;

import java.util.List;

/**
 * Created by CN.
 */
public abstract class ADBaseActivity extends SZBaseActivity {

    public boolean isShownInteractionAd = false;
    public boolean isStop = false;

    public void showInteractionAd(String codeId, Class<?> classVip) {
        showInteractionAd(codeId,classVip,true);
    }
    public void showInteractionAd(String codeId, Class<?> classVip,boolean isOnceShowVip) {
        try {
            if(isShowAd()) {
                if (!isStop && !isShownInteractionAd) {
                    if (ADManager.getInstance().isShowInteractionAd(SplashClickEyeManager.getInstance().isClickEyeShown())) {
                        SZUtil.log("开始显示新插屏");
                        SplashClickEyeManager.getInstance().setClickEyeShown(false);
                        isShownInteractionAd = true;
                        new TTVideoInteractionAdLoader(getActivity(), codeId, isOnceShowVip?SZUtil.getBuyVipClass(classVip, true):classVip).loadAd();
                    } else {
                        SZUtil.log("不显示新插屏");
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void showInteractionAd(String codeId, OnAdClosedListener onAdClosedListener) {
        showInteractionAd(codeId,onAdClosedListener,true);
    }
    public void showInteractionAd(String codeId, OnAdClosedListener onAdClosedListener,boolean isOnceShowVip) {
        try {
            if(isShowAd()) {
                if (!isStop && !isShownInteractionAd) {
                    if (ADManager.getInstance().isShowInteractionAd(SplashClickEyeManager.getInstance().isClickEyeShown())) {
                        SZUtil.log("开始显示新插屏");
                        SplashClickEyeManager.getInstance().setClickEyeShown(false);
                        isShownInteractionAd = true;
                        new TTVideoInteractionAdLoader(getActivity(), codeId, isOnceShowVip&&SZUtil.isFirstShowInteractionAd()?onAdClosedListener:null).loadAd();
                        SZUtil.setShownInteractionAd();
                    } else {
                        SZUtil.log("不显示新插屏");
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        if (isShowAd()) {
            SplashClickEyeManager.getInstance().initSplashClickEyeDataInTwoActivity(this);
        }
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onPause() {
        super.onPause();
        isStop = true;
    }

    @Override
    protected void onResume() {
        if (isShowAd()) {
            SplashClickEyeManager.getInstance().initSplashClickEyeDataInTwoActivity(this);
        }
        isStop = false;
        super.onResume();
    }

    public abstract boolean isShowAd();
}
