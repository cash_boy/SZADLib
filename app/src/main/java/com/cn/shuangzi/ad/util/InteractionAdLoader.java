package com.cn.shuangzi.ad.util;

import android.app.Activity;
import android.view.View;

import com.bytedance.sdk.openadsdk.AdSlot;
import com.bytedance.sdk.openadsdk.DislikeInfo;
import com.bytedance.sdk.openadsdk.TTAdConstant;
import com.bytedance.sdk.openadsdk.TTAdDislike;
import com.bytedance.sdk.openadsdk.TTAdNative;
import com.bytedance.sdk.openadsdk.TTAppDownloadListener;
import com.bytedance.sdk.openadsdk.TTNativeExpressAd;
import com.cn.shuangzi.ad.util.tt.TTAdManagerHolder;
import com.cn.shuangzi.ad.view.AlertVipDialog;
import com.cn.shuangzi.util.SZUtil;

import java.util.List;

import static com.bytedance.sdk.openadsdk.TTAdLoadType.PRELOAD;

/**
 * Created by CN.
 */
public class InteractionAdLoader {
    private Activity activity;
    private TTAdNative mTTAdNative;
    private String codeId;

    private Class<?> classBuyVip;
    private OnAdClosedListener onAdClosedListener;
    public InteractionAdLoader(Activity activity,String codeId){
        mTTAdNative = TTAdManagerHolder.getInstance().createAdNative(activity);
        this.activity = activity;
        this.codeId = codeId;
    }
    public InteractionAdLoader(Activity activity,String codeId,Class<?> classBuyVip){
        mTTAdNative = TTAdManagerHolder.getInstance().createAdNative(activity);
        this.activity = activity;
        this.codeId = codeId;
        this.classBuyVip = classBuyVip;
    }
    public InteractionAdLoader(Activity activity,String codeId,OnAdClosedListener onAdClosedListener){
        mTTAdNative = TTAdManagerHolder.getInstance().createAdNative(activity);
        this.activity = activity;
        this.codeId = codeId;
        this.onAdClosedListener = onAdClosedListener;
    }
    public void loadAd(boolean isSquare){
        AdSlot adSlot = new AdSlot.Builder()
                .setCodeId(codeId) //广告位id
                .setAdCount(1) //请求广告数量为1到3条
                .setSupportDeepLink(true)
//                .setDownloadType(TTAdConstant.DOWNLOAD_TYPE_POPUP)
                .setAdLoadType(PRELOAD)//推荐使用，用于标注此次的广告请求用途为预加载（当做缓存）还是实时加载，方便后续为开发者优化相关策略
                .setExpressViewAcceptedSize(isSquare?260:200, isSquare?260:300) //期望模板广告view的size,单位dp
                .build();
        mTTAdNative.loadInteractionExpressAd(adSlot, new TTAdNative.NativeExpressAdListener() {
            @Override
            public void onError(int code, String message) {
                SZUtil.log("插屏失败->code:"+code+"|message:"+message);
            }

            @Override

            public void onNativeExpressAdLoad(List<TTNativeExpressAd> ads) {
                SZUtil.log("插屏广告数据:"+ads);
                if (ads == null || ads.size() == 0) {
                    return;
                }
                final TTNativeExpressAd ad = ads.get(0);
                bindAdListener(ad);
                ad.render();
            }
        });
    }
    private void bindAdListener(final TTNativeExpressAd ad) {

        ad.setExpressInteractionListener(new TTNativeExpressAd.AdInteractionListener() {
            @Override

            public void onAdDismiss() {
                SZUtil.log("插屏广告关闭");
                try {
                    if(classBuyVip!=null&&activity!=null&&!activity.isFinishing()) {
                        new AlertVipDialog(activity, classBuyVip).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    if(onAdClosedListener!=null){
                        onAdClosedListener.onAdClosed();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override

            public void onAdClicked(View view, int type) {
                SZUtil.log( "插屏广告被点击");
            }

            @Override

            public void onAdShow(View view, int type) {
                SZUtil.log( "插屏广告展示");
            }

            @Override
            public void onRenderFail(View view, String msg, int code) {
                SZUtil.log("插屏广告失败->"+msg + " code:" + code);
            }

            @Override
            public void onRenderSuccess(View view, float width, float height) {
                //返回view的宽高 单位 dp
                ad.showInteractionExpressAd(activity);
            }
        });
        bindDislike(ad, false);

        if (ad.getInteractionType() != TTAdConstant.INTERACTION_TYPE_DOWNLOAD) {
            return;
        }
        ad.setDownloadListener(new TTAppDownloadListener() {
            @Override
            public void onIdle() {
            }

            @Override
            public void onDownloadActive(long totalBytes, long currBytes, String fileName, String appName) {
            }

            @Override
            public void onDownloadPaused(long totalBytes, long currBytes, String fileName, String appName) {
            }

            @Override
            public void onDownloadFailed(long totalBytes, long currBytes, String fileName, String appName) {
            }

            @Override
            public void onInstalled(String fileName, String appName) {
            }

            @Override
            public void onDownloadFinished(long totalBytes, String fileName, String appName) {
            }
        });
    }


    private void bindDislike(TTNativeExpressAd ad, boolean customStyle) {
        if (customStyle) {
            //使用自定义样式
            AlertVipDialog alertVipDialog = new AlertVipDialog(activity,classBuyVip);
            DislikeInfo dislikeInfo = ad.getDislikeInfo();
            if (dislikeInfo == null || dislikeInfo.getFilterWords() == null || dislikeInfo.getFilterWords().isEmpty()) {
                return;
            }
            ad.setDislikeDialog(alertVipDialog);
            return;
        }

        ad.setDislikeCallback(activity, new TTAdDislike.DislikeInteractionCallback() {
            @Override
            public void onShow() {

            }

            @Override
            public void onSelected(int position, String value, boolean enforce) {
            }

            @Override
            public void onCancel() {
            }
        });
    }
}
