package com.cn.shuangzi.ad.util.tt;

import android.content.Context;
import android.util.Log;

import com.bytedance.sdk.openadsdk.TTAdConfig;
import com.bytedance.sdk.openadsdk.TTAdConstant;
import com.bytedance.sdk.openadsdk.TTAdManager;
import com.bytedance.sdk.openadsdk.TTAdSdk;
import com.cn.shuangzi.SZManager;
import com.cn.shuangzi.ad.ADManager;
import com.cn.shuangzi.ad.R;
import com.cn.shuangzi.util.SZUtil;

public class TTAdManagerHolder {
    //    private static TTAdManager ttAdManager;
    public static boolean isBeginInit;
    public static void init(Context context, boolean isDebug, final ADManager.OnAdInitDoneListener onAdInitDoneListener) {
        if(isBeginInit){
            return;
        }
        isBeginInit = true;
        TTAdConfig.Builder builder = new TTAdConfig.Builder()
                .appId(SZUtil.getTT_APPID(context))
                .useTextureView(false) //使用TextureView控件播放视频,默认为SurfaceView,当有SurfaceView冲突的场景，可以使用TextureView
                .titleBarTheme(TTAdConstant.TITLE_BAR_THEME_DARK)
                .allowShowNotify(true) //是否允许sdk展示通知栏提示
                .allowShowPageWhenScreenLock(true) //是否在锁屏场景支持展示广告落地页
                .debug(isDebug) //测试阶段打开，可以通过日志排查问题，上线时去除该调用
                .supportMultiProcess(true);
//            if (isAutoDownloadInWifi) {
        builder.directDownloadNetworkType(TTAdConstant.NETWORK_STATE_WIFI); //允许直接下载的网络状态集合
//            } else {
//                builder.directDownloadNetworkType();
//            }
        SZUtil.log("穿山甲版本号："+TTAdSdk.getAdManager().getSDKVersion());
        TTAdSdk.init(context, builder.build(), new TTAdSdk.InitCallback() {
            @Override
            public void success() {
                isBeginInit = false;
                if(onAdInitDoneListener!=null){
                    onAdInitDoneListener.onTTAdInitDone(true);
                }
                SZUtil.log("穿山甲初始化成功: " + TTAdSdk.isInitSuccess());
            }

            @Override
            public void fail(int code, String msg) {
                isBeginInit = false;
                if(onAdInitDoneListener!=null){
                    onAdInitDoneListener.onTTAdInitDone(false);
                }
                SZUtil.log("穿山甲初始化失败:  code = " + code + " msg = " + msg);
            }
        });
    }

    public static TTAdManager getInstance() {
        if (!TTAdSdk.isInitSuccess()) {
            init(SZManager.getInstance().getContext(), SZManager.getInstance().isDebugMode(),ADManager.getInstance().getOnAdInitDoneListener());
        }
        return TTAdSdk.getAdManager();
    }
//    public static TTAdManager getInstance() {
//        if (!TTAdSdk.isInitSuccess()) {
//            synchronized (TTAdManagerHolder.class) {
//                    init(SZManager.getInstance().getContext(),SZManager.getInstance().isDebugMode(),isAutoDownloadInWifi);
//                }
//            }
//        }
//        return ttAdManager;
//    }
}
