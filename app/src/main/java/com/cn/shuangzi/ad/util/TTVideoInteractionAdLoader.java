package com.cn.shuangzi.ad.util;

import android.app.Activity;

import com.bytedance.sdk.openadsdk.AdSlot;
import com.bytedance.sdk.openadsdk.TTAdConstant;
import com.bytedance.sdk.openadsdk.TTAdNative;
import com.bytedance.sdk.openadsdk.TTFullScreenVideoAd;
import com.cn.shuangzi.ad.util.tt.TTAdManagerHolder;
import com.cn.shuangzi.ad.view.AlertVipDialog;
import com.cn.shuangzi.util.SZUtil;

import static com.bytedance.sdk.openadsdk.TTAdLoadType.PRELOAD;

/**
 * Created by CN.
 */
public class TTVideoInteractionAdLoader {
    private Activity activity;
    private TTAdNative mTTAdNative;
    private String codeId;

    private Class<?> classBuyVip;
    private OnAdClosedListener onAdClosedListener;
    public TTVideoInteractionAdLoader(Activity activity, String codeId){
        mTTAdNative = TTAdManagerHolder.getInstance().createAdNative(activity);
        this.activity = activity;
        this.codeId = codeId;
    }
    public TTVideoInteractionAdLoader(Activity activity, String codeId, Class<?> classBuyVip){
        mTTAdNative = TTAdManagerHolder.getInstance().createAdNative(activity);
        this.activity = activity;
        this.codeId = codeId;
        this.classBuyVip = classBuyVip;
    }
    public TTVideoInteractionAdLoader(Activity activity, String codeId, OnAdClosedListener onAdClosedListener){
        mTTAdNative = TTAdManagerHolder.getInstance().createAdNative(activity);
        this.activity = activity;
        this.codeId = codeId;
        this.onAdClosedListener = onAdClosedListener;
    }
    public void loadAd(){
        AdSlot adSlot = new AdSlot.Builder()
                .setCodeId(codeId)
                //模板广告需要设置期望个性化模板广告的大小,单位dp,激励视频场景，只要设置的值大于0即可
                .setExpressViewAcceptedSize(160,200)
                .setSupportDeepLink(true)
//                .setDownloadType(TTAdConstant.DOWNLOAD_TYPE_POPUP)
                .setOrientation(TTAdConstant.VERTICAL)//必填参数，期望视频的播放方向：TTAdConstant.HORIZONTAL 或 TTAdConstant.VERTICAL
                .setAdLoadType(PRELOAD)//推荐使用，用于标注此次的广告请求用途为预加载（当做缓存）还是实时加载，方便后续为开发者优化相关策略
                .build();
        mTTAdNative.loadFullScreenVideoAd(adSlot, new TTAdNative.FullScreenVideoAdListener() {
                    //请求广告失败
                    @Override
                    public void onError(int code, String message) {
                        SZUtil.log("新插屏失败->code:"+code+"|message:"+message);

                    }

                    //广告物料加载完成的回调
                    @Override
                    public void onFullScreenVideoAdLoad(TTFullScreenVideoAd ad) {

                    }

                    //广告视频/图片加载完成的回调，接入方可以在这个回调后展示广告
                    @Override
                    public void onFullScreenVideoCached() {

                    }

                    @Override
                    public void onFullScreenVideoCached(TTFullScreenVideoAd ttFullScreenVideoAd) {
                        if (ttFullScreenVideoAd != null) {
                            bindAdListener(ttFullScreenVideoAd);
                            //展示广告，并传入广告展示的场景
                            ttFullScreenVideoAd.showFullScreenVideoAd(activity, TTAdConstant.RitScenes.GAME_GIFT_BONUS, null);
                        }
                        ttFullScreenVideoAd = null;
                    }
                }
        );
    }
    private void bindAdListener(final TTFullScreenVideoAd ttFullScreenVideoAd) {
        ttFullScreenVideoAd.setFullScreenVideoAdInteractionListener(new TTFullScreenVideoAd.FullScreenVideoAdInteractionListener() {
            //广告的展示回调
            @Override
            public void onAdShow() {

            }
            //广告的下载bar点击回调
            @Override
            public void onAdVideoBarClick() {

            }
            //广告关闭的回调
            @Override
            public void onAdClose() {
                try {
                    if(classBuyVip!=null&&activity!=null&&!activity.isFinishing()) {
                        new AlertVipDialog(activity, classBuyVip).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    if(onAdClosedListener!=null){
                        onAdClosedListener.onAdClosed();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            //视频播放完毕的回调
            @Override
            public void onVideoComplete() {

            }
            //跳过视频播放
            @Override
            public void onSkippedVideo() {

            }
        });
    }

}
