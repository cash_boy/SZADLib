package com.cn.shuangzi.ad.util.tt;

import android.animation.Animator;
import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.widget.FrameLayout;

import com.bytedance.sdk.openadsdk.ISplashClickEyeListener;
import com.bytedance.sdk.openadsdk.TTSplashAd;
import com.cn.shuangzi.SZApp;
import com.cn.shuangzi.ad.ADManager;

import java.lang.ref.SoftReference;


public class SplashClickEyeManager {
    public static final int LEFT = 0;
    public static final int RIGHT = 1;
    private int mClickEyeViewWidth;//悬浮窗的宽度
    private int mClickEyeViewHeight;//悬浮窗的高度
    private int mClickEyeViewMargin;//悬浮窗最小离屏幕边缘的距离
    private int mClickEyeViewMarginBottom;//悬浮窗默认距离屏幕底端的高度
    private int mClickEyeViewPos;//悬浮窗默认位于屏幕左面或右面
    private int mClickEyeViewAnimationTime;//悬浮窗缩放动画的，单位ms
    private TTSplashAd mSplashAd;
    private View mSplashShowView;
    private int[] mOriginSplashPos = new int[2];
    private int mDecorViewWidth;
    private int mDecorViewHeight;
    private volatile static SplashClickEyeManager mInstance;
    private boolean mIsSupportSplashClickEye = false;

    private boolean isClickEyeShown = false;
    public interface AnimationCallBack {
        void animationStart(int animationTime);

        void animationEnd();
    }

    /**
     * 单例获取SplashClickEyeManager对象
     *
     * @return
     */
    public static SplashClickEyeManager getInstance() {
        if (mInstance == null) {
            synchronized (SplashClickEyeManager.class) {
                if (mInstance == null) {
                    mInstance = new SplashClickEyeManager();
                }
            }
        }
        return mInstance;
    }

    private SplashClickEyeManager() {
        Context context = SZApp.getInstance().getApplicationContext();
        initClickEyeViewData(context);
        mClickEyeViewMargin = UIUtils.dp2px(context, 10);
        mClickEyeViewMarginBottom = UIUtils.dp2px(context, 100);
        mClickEyeViewPos = RIGHT;
        mClickEyeViewAnimationTime = 600;
    }

    private void initClickEyeViewData(Context context) {
        int deviceWidth = Math.min(UIUtils.getScreenHeightInPx(context),UIUtils.getScreenWidthInPx(context));
        if (mSplashAd != null && mSplashAd.getSplashClickEyeSizeToDp() != null) {
            //使用推荐的点睛宽高
            mClickEyeViewWidth = UIUtils.dp2px(context, mSplashAd.getSplashClickEyeSizeToDp()[0]);
            mClickEyeViewHeight = UIUtils.dp2px(context, mSplashAd.getSplashClickEyeSizeToDp()[1]);
        } else {
            //默认的点睛宽高
            mClickEyeViewWidth = Math.round(deviceWidth * 0.2f);//屏幕宽度的30%，之前使用PxUtils.dpToPx(context, 90);
            mClickEyeViewHeight = Math.round(mClickEyeViewWidth * 16 / 9.0f);//根据宽度计算高度，之前使用PxUtils.dpToPx(context, 160);
        }
    }

    public void setSplashInfo(TTSplashAd splashAd, View splashView, View decorView) {
        this.mSplashAd = splashAd;
        this.mSplashShowView = splashView;
        splashView.getLocationOnScreen(mOriginSplashPos);
        mDecorViewWidth = decorView.getWidth();
        mDecorViewHeight = decorView.getHeight();
        initClickEyeViewData(SZApp.getInstance().getApplicationContext());
    }

    public void clearSplashStaticData() {
//        mSplashAd = null;
//        mSplashShowView = null;
    }

    public TTSplashAd getSplashAd() {
        return mSplashAd;
    }

    public ViewGroup startSplashClickEyeAnimationInTwoActivity(final ViewGroup decorView,
                                                               final ViewGroup splashViewContainer,
                                                               final AnimationCallBack callBack) {
        if (decorView == null || splashViewContainer == null) {
            return null;
        }
        if (mSplashAd == null || mSplashShowView == null) {
            return null;
        }
        return startSplashClickEyeAnimation(mSplashShowView, decorView, splashViewContainer, callBack);
    }

    //开屏点睛动画
    public ViewGroup startSplashClickEyeAnimation(final View splash, final ViewGroup decorView,
                                                  final ViewGroup splashViewContainer,
                                                  final AnimationCallBack callBack) {
        if (splash == null || splashViewContainer == null) {
            return null;
        }
        final int[] splashScreenPos = new int[2];
        splash.getLocationOnScreen(splashScreenPos);
        final Context context = splashViewContainer.getContext();
        int splashViewWidth = splash.getWidth();
        int splashViewHeight = splash.getHeight();
        int animationContainerWidth = decorView.getWidth();
        int animationContainerHeight = decorView.getHeight();

        if (animationContainerWidth == 0) {
            animationContainerWidth = mDecorViewWidth;
        }
        if (animationContainerHeight == 0) {
            animationContainerHeight = mDecorViewHeight;
        }
        float xScaleRatio = (float) mClickEyeViewWidth / splashViewWidth;
        float yScaleRation = (float) mClickEyeViewHeight / splashViewHeight;
        final float animationDistX = mClickEyeViewPos == LEFT ? mClickEyeViewMargin :
                animationContainerWidth - mClickEyeViewMargin - mClickEyeViewWidth;
        final float animationDistY = animationContainerHeight - mClickEyeViewMarginBottom - mClickEyeViewHeight;  //最终位于container的y坐标
        UIUtils.removeFromParent(splash);
        FrameLayout.LayoutParams animationParams = new FrameLayout.LayoutParams(splashViewWidth, splashViewHeight);
        decorView.addView(splash, animationParams);
        final FrameLayout splashViewLayout = new FrameLayout(context);
        splash.setPivotX(0);
        splash.setPivotY(0);
        splash.animate()
                .scaleX(xScaleRatio)
                .scaleY(yScaleRation)
                .x(animationDistX)
                .y(animationDistY)
                .setInterpolator(new OvershootInterpolator(0))
                .setDuration(mClickEyeViewAnimationTime)
                .setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                        if (callBack != null) {
                            callBack.animationStart(mClickEyeViewAnimationTime);
                        }
                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        UIUtils.removeFromParent(splash);
                        splash.setScaleX(1);
                        splash.setScaleY(1);
                        splash.setX(0);
                        splash.setY(0);
                        int[] clickEyeContainerScreenPos = new int[2];
                        splashViewContainer.getLocationOnScreen(clickEyeContainerScreenPos);
                        float distX = animationDistX - clickEyeContainerScreenPos[0] + splashScreenPos[0];
                        float distY = animationDistY - clickEyeContainerScreenPos[1] + splashScreenPos[1];

                        splashViewLayout.addView(splash, FrameLayout.LayoutParams.MATCH_PARENT,
                                FrameLayout.LayoutParams.MATCH_PARENT);
                        FrameLayout.LayoutParams clickEyeParams = new FrameLayout.LayoutParams(mClickEyeViewWidth,
                                mClickEyeViewHeight);
                        splashViewContainer.addView(splashViewLayout, clickEyeParams);
                        splashViewLayout.setTranslationX(distX);
                        splashViewLayout.setTranslationY(distY);
                        if (callBack != null) {
                            callBack.animationEnd();
                        }
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {
                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {
                    }
                });
        return splashViewLayout;
    }
    private View addSplashClickEyeView(Activity activity) {
        final SplashClickEyeManager splashClickEyeManager = SplashClickEyeManager.getInstance();
        final TTSplashAd splashAd = splashClickEyeManager.getSplashAd();
        return splashClickEyeManager.startSplashClickEyeAnimationInTwoActivity((ViewGroup) activity.getWindow().getDecorView(),
                (ViewGroup) activity.findViewById(android.R.id.content), new AnimationCallBack() {
                    @Override
                    public void animationStart(int animationTime) {
                    }

                    @Override
                    public void animationEnd() {
                        splashAd.splashClickEyeAnimationFinish();
                    }
                });
    }
    public void initSplashClickEyeDataInTwoActivity(Activity activity) {
        boolean isSupportSplashClickEye = isSupportSplashClickEye();
        if (!isSupportSplashClickEye) {
            clearSplashStaticData();
            return;
        }
        setClickEyeShown(true);
        View splashClickEyeView = addSplashClickEyeView(activity);
        if(splashClickEyeView != null){
            activity.overridePendingTransition(0,0);
        }
        TTSplashAd splashAd = getSplashAd();
        SplashClickEyeListener splashClickEyeListener = new SplashClickEyeListener(splashClickEyeView, splashAd);
        if (splashAd != null) {
            splashAd.setSplashClickEyeListener(splashClickEyeListener);
        }
        setSupportSplashClickEye(false);
    }

    class SplashClickEyeListener implements ISplashClickEyeListener {

        private SoftReference<View> mSplashView;
        private SoftReference<TTSplashAd> mSplashAd;

        public SplashClickEyeListener(View splashView, TTSplashAd splashAd) {
            mSplashView = new SoftReference<>(splashView);
            mSplashAd = new SoftReference<>(splashAd);
        }

        @Override
        public void onSplashClickEyeAnimationStart() {
        }

        @Override
        public void onSplashClickEyeAnimationFinish() {
            //接收点击关闭按钮的事件将开屏点睛移除。
            if (mSplashView != null && mSplashView.get() != null) {
                mSplashView.get().setVisibility(View.GONE);
                UIUtils.removeFromParent(mSplashView.get());
//                mSplashView = null;
//                mSplashAd = null;
            }
            clearSplashStaticData();
        }

        @Override
        public boolean isSupportSplashClickEye(boolean isSupport) {
            return false;
        }
    }
    public boolean isSupportSplashClickEye() {
        return mIsSupportSplashClickEye;
    }

    public void setSupportSplashClickEye(boolean isSupportSplashClickEye) {
        this.mIsSupportSplashClickEye = isSupportSplashClickEye;
    }

    public boolean isClickEyeShown() {
        return isClickEyeShown;
    }

    public void setClickEyeShown(boolean clickEyeShown) {
        isClickEyeShown = clickEyeShown;
    }

    public void setClickEyeViewPos(int mClickEyeViewPos) {
        this.mClickEyeViewPos = mClickEyeViewPos;
    }

    public void setClickEyeViewMargin(int mClickEyeViewMargin) {
        this.mClickEyeViewMargin = mClickEyeViewMargin;
    }

    public void setClickEyeViewMarginBottom(int mClickEyeViewMarginBottom) {
        this.mClickEyeViewMarginBottom = mClickEyeViewMarginBottom;
    }
}
