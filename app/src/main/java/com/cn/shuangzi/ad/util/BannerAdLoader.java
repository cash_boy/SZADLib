package com.cn.shuangzi.ad.util;

import android.app.Activity;
import android.graphics.Point;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.bytedance.sdk.openadsdk.AdSlot;
import com.bytedance.sdk.openadsdk.TTAdConstant;
import com.bytedance.sdk.openadsdk.TTAdDislike;
import com.bytedance.sdk.openadsdk.TTAdNative;
import com.bytedance.sdk.openadsdk.TTNativeExpressAd;
import com.cn.shuangzi.ad.bean.ADPlatform;
import com.cn.shuangzi.ad.util.ADPolicy;
import com.cn.shuangzi.ad.util.tt.TTAdManagerHolder;
import com.cn.shuangzi.util.SZUtil;
import com.qq.e.ads.banner2.UnifiedBannerADListener;
import com.qq.e.ads.banner2.UnifiedBannerView;
import com.qq.e.comm.util.AdError;

import java.util.List;

/**
 * Created by CN.
 */

public class BannerAdLoader {
    private ViewGroup bannerContainer;
    private String gdtBannerPosId;
    private String ttBannerCodeId;
    private int width;
    private int height;
    private Activity activity;
    private UnifiedBannerView bv;
    private TTAdNative mTTAdNative;
    private ADPolicy adPolicy;
    private TTNativeExpressAd mTTAd;
    private OnBannerAdClosedListener onBannerAdClosedListener;
    private boolean isLoadSuccess;
    private boolean isRealClose;

    public BannerAdLoader(Activity activity, String gdtBannerPosId, String ttBannerCodeId, ViewGroup bannerContainer, int width, int height, boolean isRealClose, OnBannerAdClosedListener onBannerAdClosedListener) {
        this.gdtBannerPosId = gdtBannerPosId;
        this.ttBannerCodeId = ttBannerCodeId;
        this.height = height;
        this.width = width;
        this.bannerContainer = bannerContainer;
        this.activity = activity;
        this.isRealClose = isRealClose;
        this.onBannerAdClosedListener = onBannerAdClosedListener;
        isLoadSuccess = false;
        adPolicy = new ADPolicy(activity, ADPlatform.AD_TYPE_NATIVE, new ADPolicy.OnADLoadListener() {
            @Override
            public void onLoadGDTAD() {
                isLoadSuccess = false;
                loadBannerGDT();
            }

            @Override
            public void onLoadTTAD() {
                isLoadSuccess = false;
                loadBannerTT();
            }

            @Override
            public void onLoadData() {

            }
        });
    }

    public BannerAdLoader(Activity activity, String gdtBannerPosId, String ttBannerCodeId, ViewGroup bannerContainer, boolean isRealClose, OnBannerAdClosedListener onBannerAdClosedListener) {
        this(activity, gdtBannerPosId, ttBannerCodeId, bannerContainer, 0, 0, isRealClose, onBannerAdClosedListener);
    }

    public void loadBanner() {
        adPolicy.loadADPolicy();
    }

    public void closeBanner() {
        try {
            if (bv != null) {
                bv.destroy();
                bv = null;
                if (onBannerAdClosedListener != null) {
                    onBannerAdClosedListener.onGDTAdClosed();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            if (mTTAd != null) {
                mTTAd.destroy();
                mTTAd = null;
                if(onBannerAdClosedListener!=null){
                    onBannerAdClosedListener.onTTAdClosed();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            bannerContainer.removeAllViews();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private UnifiedBannerView getBannerGDT() {
        if (bv == null) {
            bv = new UnifiedBannerView(activity, gdtBannerPosId, new UnifiedBannerADListener() {
                @Override
                public void onNoAD(AdError adError) {
                    SZUtil.log("广点通:" + adError.getErrorMsg() + "|||" + adError.getErrorCode());
                    adPolicy.fetchNextPlatformAd();
                }

                @Override
                public void onADReceive() {

                }

                @Override
                public void onADExposure() {
                    SZUtil.log("======广点通onADExposure======");
                    isLoadSuccess = true;
                }

                @Override
                public void onADClosed() {
                    if (isRealClose) {
                        closeBanner();
                    }
                }

                @Override
                public void onADClicked() {

                }

                @Override
                public void onADLeftApplication() {

                }

            });
        }
        bannerContainer.removeAllViews();
        bannerContainer.addView(bv, getUnifiedBannerLayoutParams());
        return bv;
    }

    private FrameLayout.LayoutParams getUnifiedBannerLayoutParams() {
        if (width > 0) {
            return new FrameLayout.LayoutParams(width, height);
        }
        Point screenSize = new Point();
        activity.getWindowManager().getDefaultDisplay().getSize(screenSize);
        return new FrameLayout.LayoutParams(screenSize.x, Math.round(screenSize.x / 6.4F));
    }

    private void loadBannerGDT() {
        getBannerGDT().loadAD();
    }

    private TTAdNative getBannerTT() {
        if (mTTAdNative == null) {
            mTTAdNative = TTAdManagerHolder.getInstance().createAdNative(activity);
        }
        return mTTAdNative;
    }

    private void loadBannerTT() {
        bannerContainer.removeAllViews();
        FrameLayout.LayoutParams layoutParams = getUnifiedBannerLayoutParams();
        AdSlot adSlot = new AdSlot.Builder()
                .setCodeId(ttBannerCodeId)
                .setExpressViewAcceptedSize(SZUtil.px2dip(layoutParams.width), SZUtil.px2dip(layoutParams.height))
                .setSupportDeepLink(true)
//                .setDownloadType(TTAdConstant.DOWNLOAD_TYPE_POPUP)
                .setImageAcceptedSize(640, 100)
                .build();
        getBannerTT().loadBannerExpressAd(adSlot, new TTAdNative.NativeExpressAdListener() {
            @Override
            public void onError(int i, String s) {
                SZUtil.log("穿山甲:" + i + "|||" + s);
                adPolicy.fetchNextPlatformAd();

            }

            @Override
            public void onNativeExpressAdLoad(List<TTNativeExpressAd> list) {
                if (list == null || list.size() == 0) {
                    adPolicy.fetchNextPlatformAd();
                    return;
                }
                mTTAd = list.get(0);
                mTTAd.setSlideIntervalTime(4 * 1000);
                mTTAd.setExpressInteractionListener(new TTNativeExpressAd.ExpressAdInteractionListener() {
                    @Override
                    public void onAdClicked(View view, int i) {

                    }

                    @Override
                    public void onAdShow(View view, int i) {
                        SZUtil.log("穿山甲onAdShow====view:" + view + "|code" + i);
                        isLoadSuccess = true;
                    }

                    @Override
                    public void onRenderFail(View view, String s, int i) {
                        adPolicy.fetchNextPlatformAd();
                    }

                    @Override
                    public void onRenderSuccess(View view, float v, float v1) {
                        bannerContainer.addView(view);
                    }
                });
                boolean isDeal = false;
                if (onBannerAdClosedListener != null) {
                    isDeal = onBannerAdClosedListener.onTTAdDislike(bannerContainer,mTTAd);
                }
                if(!isDeal){
                    mTTAd.setDislikeCallback(activity, new TTAdDislike.DislikeInteractionCallback() {
                        @Override
                        public void onShow() {
                        }

                        @Override
                        public void onSelected(int position, String value, boolean enforce) {
                            if (isRealClose) {
                                closeBanner();
                            }
                        }

                        @Override
                        public void onCancel() {
                        }

                    });
                }
                mTTAd.render();
            }
        });
    }

    public boolean isLoadSuccess() {
        return isLoadSuccess;
    }

    public void setLoadSuccess(boolean loadSuccess) {
        isLoadSuccess = loadSuccess;
    }

    public interface OnBannerAdClosedListener {

        boolean onTTAdDislike(ViewGroup bannerContainer, TTNativeExpressAd ad);

        void onTTAdClosed();

        void onGDTAdClosed();
    }

}
