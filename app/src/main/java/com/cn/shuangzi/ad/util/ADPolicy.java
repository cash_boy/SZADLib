package com.cn.shuangzi.ad.util;

import android.content.Context;
import android.text.TextUtils;

import com.cn.shuangzi.util.SZValidatorUtil;
import com.cn.shuangzi.util.SZXmlUtil;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.cn.shuangzi.ad.bean.ADPlatform;

import java.util.List;

import static com.cn.shuangzi.ad.util.ADConst.AD_LOAD_NATIVE_POLICY;
import static com.cn.shuangzi.ad.util.ADConst.AD_LOAD_POLICY;
import static com.cn.shuangzi.ad.util.ADConst.AD_LOAD_SPLASH_POLICY;

/**
 * Created by CN.
 */

public class ADPolicy {
    private Context context;
    private List<ADPlatform> adPlatformList;
    private String adType;
    public ADPolicy(Context context, String adType, OnADLoadListener onADLoadListener){
        this.context = context;
        this.onADLoadListener = onADLoadListener;
        this.adType = adType;
    }
    public void loadADPolicy() {
        SZXmlUtil sZXmlUtil = new SZXmlUtil(context, AD_LOAD_POLICY);
        String data = sZXmlUtil.getString(ADPlatform.AD_TYPE_SPLASH.equalsIgnoreCase(adType) ? AD_LOAD_SPLASH_POLICY : AD_LOAD_NATIVE_POLICY);
        if(("[]".equals(data)||TextUtils.isEmpty(data))&&ADPlatform.AD_TYPE_NATIVE.equals(adType)){
            data = sZXmlUtil.getString(AD_LOAD_SPLASH_POLICY);
        }
        if (!TextUtils.isEmpty(data)&&!"[]".equals(data)) {
            try {
                adPlatformList = new Gson().fromJson(data, new TypeToken<List<ADPlatform>>() {
                }.getType());
                onFetchAdSuccess();
                return;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if(onADLoadListener!=null){
            onADLoadListener.onLoadData();
        }
    }
    private void onFetchAdSuccess() {
        if (adPlatformList != null && adPlatformList.size() > 0) {
            try {
                ADPlatform adPlatform = adPlatformList.get(ADConst.LOAD_AD_INDEX);
                if (adPlatform.isAdIsOpen()) {
                    fetchPlatformAd(adPlatform);
                } else {
                    if(onADLoadListener!=null){
                        onADLoadListener.onLoadData();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                if(onADLoadListener!=null){
                    onADLoadListener.onLoadData();
                }
            }
        } else {
            if(onADLoadListener!=null){
                onADLoadListener.onLoadData();
            }
        }
    }

    public void fetchNextPlatformAd() {
        if (adPlatformList != null && adPlatformList.size() > 0) {
            fetchPlatformAd(adPlatformList.get(0));
        } else {
            if(onADLoadListener!=null){
                onADLoadListener.onLoadData();
            }
        }
    }

    private void fetchPlatformAd(ADPlatform adPlatform) {
        adPlatformList.remove(adPlatform);
        switch (adPlatform.getAdSupportType()) {
            case ADConst.AD_PLATFORM_TT:
                if(onADLoadListener!=null){
                    onADLoadListener.onLoadTTAD();
                }
                break;
            case ADConst.AD_PLATFORM_GDT:
                if(onADLoadListener!=null){
                    onADLoadListener.onLoadGDTAD();
                }
                break;
            default:
                if(onADLoadListener!=null){
                    onADLoadListener.onLoadData();
                }
        }
    }
    private OnADLoadListener onADLoadListener;

    public ADPolicy.OnADLoadListener getOnADLoadListener() {
        return onADLoadListener;
    }

    public void setOnADLoadListener(ADPolicy.OnADLoadListener onADLoadListener) {
        this.onADLoadListener = onADLoadListener;
    }

    public interface OnADLoadListener{
        void onLoadGDTAD();
        void onLoadTTAD();
        void onLoadData();
    }
}
