package com.cn.shuangzi.ad.util.gdt;

import android.app.Activity;

import com.cn.shuangzi.ad.util.VideoAdLoader;
import com.cn.shuangzi.util.SZUtil;
import com.qq.e.ads.rewardvideo.RewardVideoAD;
import com.qq.e.ads.rewardvideo.RewardVideoADListener;
import com.qq.e.ads.rewardvideo.ServerSideVerificationOptions;
import com.qq.e.comm.listeners.NegativeFeedbackListener;
import com.qq.e.comm.util.AdError;

import java.util.Map;

/**
 * Created by CN.
 */
public class GDTVideoAdLoader {
    private String posId;
    private Activity activity;
    private RewardVideoAD rewardVideoAD;
    private VideoAdLoader.VideoPlayListener videoPlayListener;
    private boolean isRewardSuccess;
    private boolean isDealDoneAfterReward;
    public GDTVideoAdLoader(Activity activity, String posId, VideoAdLoader.VideoPlayListener videoPlayListener) {
        this.videoPlayListener = videoPlayListener;
        this.activity = activity;
        this.posId = posId;
        isRewardSuccess = false;
        isDealDoneAfterReward = false;
    }

    public void loadVideo() {
        loadVideo(null,null);
    }
    public void loadVideo(String userId,String extra) {
        isRewardSuccess = false;
        isDealDoneAfterReward = false;
        if(rewardVideoAD == null) {
            rewardVideoAD = new RewardVideoAD(activity, posId, new RewardVideoADListener() {
                @Override
                public void onADLoad() {

                    if (rewardVideoAD.getRewardAdType() == RewardVideoAD.REWARD_TYPE_VIDEO) {
                        SZUtil.logError("eCPMLevel = " + rewardVideoAD.getECPMLevel() + ", ECPM: " + rewardVideoAD.getECPM()
                                + " ,video duration = " + rewardVideoAD.getVideoDuration()
                                + ", testExtraInfo:" + rewardVideoAD.getExtraInfo().get("mp")
                                + ", request_id:" + rewardVideoAD.getExtraInfo().get("request_id"));
                    } else if (rewardVideoAD.getRewardAdType() == RewardVideoAD.REWARD_TYPE_PAGE) {
                        SZUtil.logError("eCPMLevel = " + rewardVideoAD.getECPMLevel()
                                + ", ECPM: " + rewardVideoAD.getECPM()
                                + ", testExtraInfo:" + rewardVideoAD.getExtraInfo().get("mp")
                                + ", request_id:" + rewardVideoAD.getExtraInfo().get("request_id"));
                    }
                    if (DownloadConfirmHelper.USE_CUSTOM_DIALOG) {
                        rewardVideoAD.setDownloadConfirmListener(DownloadConfirmHelper.DOWNLOAD_CONFIRM_LISTENER);
                    }
//                    reportBiddingResult(mRewardVideoAD);
                    if (GDTUtil.isAdValid(true, rewardVideoAD != null && rewardVideoAD.isValid(), true)) {
                        rewardVideoAD.showAD();
                    }else{
                        if(videoPlayListener!=null){
                            videoPlayListener.onError();
                        }
                    }
                }

                @Override
                public void onVideoCached() {

                }

                @Override
                public void onADShow() {
                    SZUtil.log("展示广告了=====onAdShow======");
                    if(videoPlayListener!=null){
                        videoPlayListener.onShow();
                    }
                }

                @Override
                public void onADExpose() {

                }

                @Override
                public void onReward(Map<String, Object> map) {
                    Object id = map.get(ServerSideVerificationOptions.TRANS_ID);
                    SZUtil.logError("onReward TRANS_ID：" + id);// 获取服务端验证的唯一 ID
                    if(videoPlayListener!=null){
//                        String code = null;
//                        if(id!=null && id instanceof String){
//                            code = (String) id;
//                            if(!SZValidatorUtil.isValidString(code)){
//                                code = null;
//                            }
//                        }
                        if(id!=null) {
                            isRewardSuccess = true;
                            videoPlayListener.onSuccess();
                        }else{
                            isRewardSuccess = false;
                            videoPlayListener.onError();
                        }
                    }
                }

                @Override
                public void onADClick() {

                }

                @Override
                public void onVideoComplete() {

                }

                @Override
                public void onADClose() {
                    SZUtil.log("关闭广告了=====onAdClose======");
                    if(videoPlayListener!=null){
                        videoPlayListener.onClose();
                    }
                }

                @Override
                public void onError(AdError adError) {
                    SZUtil.logError("code:"+adError.getErrorMsg()+"||msg:"+adError.getErrorMsg());
                    if(videoPlayListener!=null){
                        videoPlayListener.onError();
                    }
                }
            }, true);
        }
        rewardVideoAD.setNegativeFeedbackListener(new NegativeFeedbackListener() {
            @Override
            public void onComplainSuccess() {
                SZUtil.logError("onComplainSuccess");
            }
        });
        ServerSideVerificationOptions options = new ServerSideVerificationOptions.Builder()
                .setCustomData(extra) // 设置激励视频服务端验证的自定义信息
                .setUserId(userId) // 设置服务端验证的用户信息
                .build();
        rewardVideoAD.setServerSideVerificationOptions(options);
        rewardVideoAD.loadAD();
//        Map<String, String> info = new HashMap<>();
//        info.put("custom_key", "reward_video");
//        LoadAdParams loadAdParams = new LoadAdParams();
//        loadAdParams.setDevExtra(info);
//        return loadAdParams;
//        rewardVideoAD.setLoadAdParams(DemoUtil.getLoadAdParams());
    }

    public void setDealDoneAfterReward(boolean dealDoneAfterReward) {
        isDealDoneAfterReward = dealDoneAfterReward;
    }

    public boolean isDealDoneAfterReward() {
        return isDealDoneAfterReward;
    }

    public boolean isRewardSuccess() {
        return isRewardSuccess;
    }

    public void setRewardSuccess(boolean rewardSuccess) {
        isRewardSuccess = rewardSuccess;
    }
}
