package com.cn.shuangzi.ad.view;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;

import com.bytedance.sdk.openadsdk.TTDislikeDialogAbstract;
import com.cn.shuangzi.SZBaseActivity;
import com.cn.shuangzi.ad.R;
import com.cn.shuangzi.ad.util.BannerAdLoader;

import androidx.annotation.NonNull;

/**
 * Created by CN.
 */

public class AlertVipDialog extends TTDislikeDialogAbstract {
    private SZBaseActivity baseActivity;
    private Class<?> classBuyVip;
    private BannerAdLoader bannerAdLoader;
    public AlertVipDialog(@NonNull Context context, Class<?> classBuyVip) {
        this(context,classBuyVip,null);
    }
    public AlertVipDialog(@NonNull Context context, Class<?> classBuyVip,BannerAdLoader bannerAdLoader) {
        super(context, R.style.alert_dialog);
        this.classBuyVip = classBuyVip;
        this.bannerAdLoader = bannerAdLoader;
        setCancelable(true);
        setCanceledOnTouchOutside(false);
        this.baseActivity = (SZBaseActivity) context;
    }
    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        findViewById(R.id.cancel_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancel();
            }
        });
        findViewById(R.id.confirm_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancel();
                baseActivity.startActivity(classBuyVip);
            }
        });
        if(bannerAdLoader!=null){
            bannerAdLoader.closeBanner();
        }
    }

    @Override
    public int getLayoutId() {
        return R.layout.alert_dislike_dialog;
    }

    @Override
    public int[] getTTDislikeListViewIds() {
        return new int[]{R.id.lv_dislike_custom};
    }

//    @Override
//    public int[] getPersonalizationPromptIds() {
//        return new int[]{R.id.tv_personalize_prompt};
//    }

    @Override
    public ViewGroup.LayoutParams getLayoutParams() {
        return new ViewGroup.LayoutParams(baseActivity.getResources().getDimensionPixelOffset(R.dimen.alert_width),ViewGroup.LayoutParams.WRAP_CONTENT);
    }
}
