package com.cn.shuangzi.ad.bean;

/**
 * Created by CN.
 */

public class NativePlatformInfo {
    private String platform;
    private String id;
    private int count;
    private int height;
    private int width;
    public NativePlatformInfo(String platform, String id, int count) {
        this.platform = platform;
        this.id = id;
        this.count = count;
    }

    public NativePlatformInfo(String platform, String id, int count, int width, int height) {
        this.platform = platform;
        this.id = id;
        this.count = count;
        this.width = width;
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "NativePlatformInfo{" +
                "platform='" + platform + '\'' +
                ", id='" + id + '\'' +
                ", count=" + count +
                ", height=" + height +
                ", width=" + width +
                '}';
    }
}
